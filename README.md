# pcmedkit

pcmedkit is a ultra versatile memstick, which runs out of the ramdisk (memory).
It allows to install or fix Linux, Windows (ntfs) and BSD systems out of the ram. 

It features NetBSD, Slackware and Linux Debian running on Ramdisk. 


![](pcmedkit.png)



# Quick Start (for modern PCs, AMD64., Ryzen,... 2021)


1.) Fetch the file:

 wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/release-v5/pcmedkit-memstick-amd64-v5-release-ramdisk-devuan-200mb.img.gz"


2.) The regular method:

zcat pcmedkit-memstick-amd64-v5-release-ramdisk-devuan-200mb.img.gz  > /dev/sdX 

3.) Boot your pendrive and select using GRUB the required system to recover or to partition your disk. 

The available systems are live running out of mem. 



# Packages on memstick/hdd image

The C compiler (gcc) with make to allow kernel compilation (installed on second partition).

It has as well: 
Grub,
debootstrap,
vim,
partimage, 
links,
mpg123,
...

The releases are mostly designed without desktop (X11) in order to have smaller size. 





# Workaround using NetBSD live  (tested and very stable)

Image: 
 "https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/release-v5/pcmedkit-memstick-amd64-v5-release-ramdisk-devuan-200mb.img.gz"


It includes a grub-install if you would like to bring back a new grub on your mbr. 






# Quick Start (for older PCs,  X86., Intel Pentium 1,2,3, First Generation)

1.) Fetch the file: 
https://gitlab.com/openbsd98324/pcmedkit/-/blob/master/release/pcmedkit-memstick-x86-v1-release-ramdisk.img.gz

2.) The regular method:

zcat pcmedkit-memstick-x86-v1-release-ramdisk.img.gz > /dev/sdX 






# History, List with Releases

v1: x86 small size for Pentium machines (486), based on Debian, with grub 1.99, netbsd install (boot.cfg) with NetBSD 9.1 i386, on partition 1., and slackware, and a Linux.  A NetBSD 7.1.1. can be still possible to be added, if the PC is less than 16 MB. 

v2: x86 modern machines (x86, i386, 686), based on Devuan

v3: modern machines (AMD64), based on Devuan about 300MB, minimal system

v4: modern machines (AMD64), based on Devuan about 400MB, with rescue disk apps like testdisk and partimage (...).
The release v4 (plus) has sufficient utils to fix a machine, using xfs,... and as well ntfs-3g.
partimage is available in order to backup MS Windows 7, 8 and 10 to external harddisk. Since it runs out of the memory, only a single USB port might be needed (ext. harddisk). It runs as well NetBSD and Slackware to ramdisk (which might explain the large size of 400 MB).
Content: C compiler (gcc and make), debootstrap, wpa_supplicant for wifi, fdisk, ifconfig, ifupdown, testdisk, vim, mpg123, partimage, xfsprogs, ntfs-3g, wodim, links, nano, ..

v5 *(Stable, Current)*: AMD64, Small size, Rescue on 200 MB Only. To save space, ext2 only is provided.

v6 and v7: testing releases, PCMedKit with GPT / EFI to boot

(...)



# How to use Partimage


- To backup (_aka_. clone) a partition, example /dev/sda4:

> partclone.ntfs  -c -d -s /dev/sda4  -o partclone.sda4.ntfs.img 


- To restore a partition, example /dev/sda4:
 
> partclone.ntfs -r -d -s partclone.sda4.ntfs.img -o  /dev/sda4  


# Screenshot

![](https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/media/screenshot/pcmedkit-v4-screenshot01.png)

![](https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/media/screenshot/pcmedkit-v4-screenshot03.png)

![](https://gitlab.com/openbsd98324/pcmedkit/-/raw/master/media/screenshot/pcmedkit-v4-screenshot02.png)



# References 

[1.] For a system running grub and netbsd - live, see: netbsd-live 
    URL : https://gitlab.com/openbsd98324/netbsd-live


[2.] The Nano Grub for modern PCs running on 22MB only: https://gitlab.com/openbsd98324/nanogrub

[3.] For (very) old notebooks:  https://gitlab.com/openbsd98324/antic-pckit




